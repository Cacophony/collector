package main

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"gitlab.com/Cacophony/Collector/pkg/enabled"
	"gitlab.com/Cacophony/go-kit/events"
	"go.uber.org/zap"
)

func handle(
	logger *zap.Logger,
	dynamoTable string,
	dynamoDB *dynamodb.DynamoDB,
	checker *enabled.Checker,
) func(event *events.Event) error {
	return func(event *events.Event) error {
		if ignoreEvent(event) {
			logger.Debug("skipping event",
				zap.String("event_type", string(event.Type)),
				zap.String("event_id", event.ID),
			)
			return nil
		}

		if !checker.IsEnabled(event.GuildID) {
			logger.Debug("skipping event because guild is not enabled",
				zap.String("event_type", string(event.Type)),
				zap.String("event_id", event.ID),
			)
			return nil
		}

		item, err := dynamodbattribute.MarshalMap(event)
		if err != nil {
			return err
		}

		logger.Debug("storing event",
			zap.String("event_type", string(event.Type)),
			zap.String("event_id", event.ID),
		)

		_, err = dynamoDB.PutItemWithContext(context.TODO(), &dynamodb.PutItemInput{
			Item:      item,
			TableName: aws.String(dynamoTable),
		})
		return err
	}
}

func ignoreEvent(event *events.Event) bool {
	if event == nil || event.ID == "" || event.Type == "" {
		return true
	}

	if event.GuildID == "" {
		return true
	}

	if event.Type == events.MessageUpdateType &&
		event.MessageUpdate.EditedTimestamp == "" {
		return true
	}

	return false
}
