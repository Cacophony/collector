package main

import (
	"time"

	"gitlab.com/Cacophony/go-kit/errortracking"
	"gitlab.com/Cacophony/go-kit/featureflag"
	"gitlab.com/Cacophony/go-kit/logging"
)

// nolint: lll
type config struct {
	Port                      int                  `envconfig:"PORT" default:"8000"`
	Hash                      string               `envconfig:"HASH"`
	Environment               logging.Environment  `envconfig:"ENVIRONMENT" default:"development"`
	ClusterEnvironment        string               `envconfig:"CLUSTER_ENVIRONMENT" default:"development"`
	AMQPDSN                   string               `envconfig:"AMQP_DSN" default:"amqp://guest:guest@localhost:5672/"`
	LoggingDiscordWebhook     string               `envconfig:"LOGGING_DISCORD_WEBHOOK"`
	ConcurrentProcessingLimit int                  `envconfig:"CONCURRENT_PROCESSING_LIMIT" default:"50"`
	ProcessingDeadline        time.Duration        `envconfig:"PROCESSING_DEADLINE" default:"5m"`
	DBDSN                     string               `envconfig:"DB_DSN" default:"postgres://postgres:postgres@localhost:5432/?sslmode=disable"`
	RedisAddress              string               `envconfig:"REDIS_ADDRESS" default:"localhost:6379"`
	RedisPassword             string               `envconfig:"REDIS_PASSWORD"`
	FeatureFlag               featureflag.Config   `envconfig:"FEATUREFLAG"`
	ErrorTracking             errortracking.Config `envconfig:"ERRORTRACKING"`
	AWS                       awsConfig            `envconfig:"AWS"`
	EventlogTable             string               `envconfig:"EVENTLOG_TABLE" default:"eventlog"`
}

type awsConfig struct {
	AccessKeyID     string `envconfig:"ACCESS_KEY_ID"`
	SecretAccessKey string `envconfig:"SECRET_ACCESS_KEY"`
	Token           string `envconfig:"TOKEN"`
	Region          string `envconfig:"REGION"`
}
