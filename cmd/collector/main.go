package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/Cacophony/Collector/pkg/enabled"
	"gitlab.com/Cacophony/go-kit/events"

	"github.com/aws/aws-sdk-go/service/dynamodb"

	"github.com/aws/aws-sdk-go/aws/credentials"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
	"gitlab.com/Cacophony/go-kit/api"
	"gitlab.com/Cacophony/go-kit/errortracking"
	"gitlab.com/Cacophony/go-kit/logging"
	"go.uber.org/zap"
)

const (
	// ServiceName is the name of the service
	ServiceName = "collector"
)

func main() {
	// init config
	var config config
	err := envconfig.Process("", &config)
	if err != nil {
		panic(errors.Wrap(err, "unable to load configuration"))
	}
	config.FeatureFlag.Environment = config.ClusterEnvironment
	config.ErrorTracking.Version = config.Hash
	config.ErrorTracking.Environment = config.ClusterEnvironment

	// init logger
	logger, err := logging.NewLogger(
		config.Environment,
		ServiceName,
		config.LoggingDiscordWebhook,
		&http.Client{
			Timeout: 10 * time.Second,
		},
	)
	if err != nil {
		panic(errors.Wrap(err, "unable to initialise launcher"))
	}

	// init raven
	err = errortracking.Init(&config.ErrorTracking)
	if err != nil {
		logger.Error("unable to initialise errortracking",
			zap.Error(err),
		)
	}

	// init redis
	redisClient := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddress,
		Password: config.RedisPassword,
	})
	_, err = redisClient.Ping().Result()
	if err != nil {
		logger.Fatal("unable to connect to Redis",
			zap.Error(err),
		)
	}

	// init GORM
	gormDB, err := gorm.Open("postgres", config.DBDSN)
	if err != nil {
		logger.Fatal("unable to initialise GORM session",
			zap.Error(err),
		)
	}
	// gormDB.SetLogger(logger) TODO: write logger
	defer gormDB.Close()

	// // init feature flagger
	// featureFlagger, err := featureflag.New(&config.FeatureFlag)
	// if err != nil {
	// 	logger.Fatal("unable to initialise feature flagger",
	// 		zap.Error(err),
	// 	)
	// }

	// init AWS DynamoDB
	awsSession, err := session.NewSession(
		&aws.Config{
			Region: aws.String(config.AWS.Region),
			Credentials: credentials.NewStaticCredentials(
				config.AWS.AccessKeyID, config.AWS.SecretAccessKey, config.AWS.Token,
			),
		},
	)
	if err != nil {
		logger.Fatal("unable to initialise AWS session",
			zap.Error(err),
		)
	}
	dynamoDB := dynamodb.New(awsSession)

	// init enabled checker
	checker := enabled.NewChecker(
		logger.With(zap.String("feature", "enabled_checker")),
		gormDB,
		time.Minute,
	)
	err = checker.Start()
	if err != nil {
		logger.Fatal("unable to start enabled checker",
			zap.Error(err),
		)
	}

	// init handler
	handler := handle(
		logger,
		config.EventlogTable,
		dynamoDB,
		checker,
	)

	// init processor
	processor, err := events.NewConsumer(
		logger,
		ServiceName,
		config.AMQPDSN,
		config.ConcurrentProcessingLimit,
		handler,
	)
	if err != nil {
		logger.Fatal("unable to initialise processor",
			zap.Error(err),
		)
	}

	// init http server
	httpRouter := api.NewRouter()
	httpServer := api.NewHTTPServer(config.Port, httpRouter)

	// start processor
	go func() {
		err := processor.Start(context.TODO())
		if err != nil {
			logger.Fatal("processor error received", zap.Error(err))
		}
	}()

	// start http server
	go func() {
		err := httpServer.ListenAndServe()
		if err != http.ErrServerClosed {
			logger.Fatal("http server error",
				zap.Error(err),
				zap.String("feature", "http-server"),
			)
		}
	}()

	logger.Info("service is running",
		zap.Int("port", config.Port),
		zap.Int("concurrent_processing_limit", config.ConcurrentProcessingLimit),
		zap.String("environment", string(config.Environment)),
	)

	// wait for CTRL+C to stop the service
	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-quitChannel

	// shutdown features

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	// TODO: make sure processor is finished processing events before shutting down

	err = httpServer.Shutdown(ctx)
	if err != nil {
		logger.Error("unable to shutdown HTTP Server",
			zap.Error(err),
		)
	}

}
