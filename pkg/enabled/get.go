package enabled

import (
	"strings"

	"gitlab.com/Cacophony/go-kit/config"
)

const chatlogEnabledKey = "cacophony:chatlog:enabled"

func (c *Checker) getServerIDs() (map[string]interface{}, error) {
	var servers []config.Item

	err := c.db.Model(config.Item{}).Where(
		"key = ? AND value = ?",
		chatlogEnabledKey,
		[]byte{uint8(1)},
	).Find(&servers).Error
	if err != nil {
		return nil, err
	}

	serverList := make([]string, len(servers))
	for i, server := range servers {
		parts := strings.Split(server.Namespace, ":")
		if len(parts) < 2 {
			continue
		}
		serverList[i] = parts[1]
	}

	return sliceIntoMap(serverList), nil
}

func sliceIntoMap(list []string) map[string]interface{} {
	ids := make(map[string]interface{})

	for _, item := range list {
		ids[item] = true
	}

	return ids
}
