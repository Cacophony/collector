package enabled

import (
	"sync"
	"time"

	raven "github.com/getsentry/raven-go"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"go.uber.org/zap"
)

type Checker struct {
	logger   *zap.Logger
	db       *gorm.DB
	interval time.Duration

	serverlist     map[string]interface{}
	serverlistLock sync.RWMutex
}

func NewChecker(
	logger *zap.Logger,
	db *gorm.DB,
	interval time.Duration,
) *Checker {
	return &Checker{
		logger:   logger,
		db:       db,
		interval: interval,
	}
}

func (c *Checker) Start() error {
	var err error

	c.serverlistLock.Lock()
	c.serverlist, err = c.getServerIDs()
	c.serverlistLock.Unlock()
	if err != nil && err != redis.Nil {
		return err
	}

	go func() {
		var err error
		var serverlist map[string]interface{}
		for {
			time.Sleep(c.interval)

			serverlist, err = c.getServerIDs()
			if err != nil && err != redis.Nil {
				raven.CaptureError(err, nil)
				c.logger.Error("failed to retrieve serverlisted", zap.Error(err))
			}

			c.serverlistLock.Lock()
			c.serverlist = serverlist
			c.serverlistLock.Unlock()

			c.logger.Debug("cached serverlist")
		}
	}()

	return nil
}

func (c *Checker) IsEnabled(guildID string) bool {
	c.serverlistLock.RLock()
	defer c.serverlistLock.RUnlock()

	if c.serverlist == nil {
		return false
	}

	_, ok := c.serverlist[guildID]
	return ok
}
