#!/usr/bin/env bash

# should have the following environment variables set:
# PORT
# HASH
# ENVIRONMENT
# CLUSTER_ENVIRONMENT
# AMQP_DSN
# LOGGING_DISCORD_WEBHOOK
# DOCKER_IMAGE_HASH
# CONCURRENT_PROCESSING_LIMIT
# DB_DSN
# REDIS_ADDRESS
# REDIS_PASSWORD
# FEATUREFLAG_UNLEASH_URL
# FEATUREFLAG_UNLEASH_INSTANCE_ID
# ERRORTRACKING_RAVEN_DSN
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_REGION
# EVENTLOG_TABLE

template="k8s/manifest.tmpl.yaml"
target="k8s/manifest.yaml"

cp "$template" "$target"
sed -i -e "s|{{PORT}}|$PORT|g" "$target"
sed -i -e "s|{{HASH}}|$HASH|g" "$target"
sed -i -e "s|{{ENVIRONMENT}}|$ENVIRONMENT|g" "$target"
sed -i -e "s|{{CLUSTER_ENVIRONMENT}}|$CLUSTER_ENVIRONMENT|g" "$target"
sed -i -e "s|{{AMQP_DSN}}|$AMQP_DSN|g" "$target"
sed -i -e "s|{{LOGGING_DISCORD_WEBHOOK}}|$LOGGING_DISCORD_WEBHOOK|g" "$target"
sed -i -e "s|{{DOCKER_IMAGE_HASH}}|$DOCKER_IMAGE_HASH|g" "$target"
sed -i -e "s|{{CONCURRENT_PROCESSING_LIMIT}}|$CONCURRENT_PROCESSING_LIMIT|g" "$target"
sed -i -e "s|{{DB_DSN}}|$DB_DSN|g" "$target"\
sed -i -e "s|{{REDIS_ADDRESS}}|$REDIS_ADDRESS|g" "$target"
sed -i -e "s|{{REDIS_PASSWORD}}|$REDIS_PASSWORD|g" "$target"
sed -i -e "s|{{FEATUREFLAG_UNLEASH_URL}}|$FEATUREFLAG_UNLEASH_URL|g" "$target"
sed -i -e "s|{{FEATUREFLAG_UNLEASH_INSTANCE_ID}}|$FEATUREFLAG_UNLEASH_INSTANCE_ID|g" "$target"
sed -i -e "s|{{ERRORTRACKING_RAVEN_DSN}}|$ERRORTRACKING_RAVEN_DSN|g" "$target"
sed -i -e "s|{{AWS_ACCESS_KEY_ID}}|$AWS_ACCESS_KEY_ID|g" "$target"
sed -i -e "s|{{AWS_SECRET_ACCESS_KEY}}|$AWS_SECRET_ACCESS_KEY|g" "$target"
sed -i -e "s|{{AWS_REGION}}|$AWS_REGION|g" "$target"
sed -i -e "s|{{EVENTLOG_TABLE}}|$EVENTLOG_TABLE|g" "$target"
