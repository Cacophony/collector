module gitlab.com/Cacophony/Collector

require (
	github.com/aws/aws-sdk-go v1.19.46
	github.com/bwmarrin/discordgo v0.16.1-0.20190826195003-1d90c5da95a4 // indirect
	github.com/getsentry/raven-go v0.2.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/jinzhu/gorm v1.9.8
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/pkg/errors v0.8.1
	gitlab.com/Cacophony/go-kit v0.0.0-20190904142828-c67e00f5173c
	go.uber.org/zap v1.10.0
)

go 1.13
